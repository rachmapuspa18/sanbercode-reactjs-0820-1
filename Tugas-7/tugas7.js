// Soal 1
console.log('-----Soal 1-----')
console.log('-----Release 0-----')
class Animal 
{
    constructor(name,legs,cold_blooded) 
    {
        this.name = name;
        this.legs = legs;
        this.cold_blooded = cold_blooded;
    }
    get anam ()
    {
        return this.name;
        return this.legs;
        return this.cold_blooded;
    }
    set anam (_x)
    {
        this.name = _x;
        this.legs = _x;
        this.cold_blooded = _x;
    }
}
var sheep = new Animal("shaun","4","false");

console.log(sheep.name) 
console.log(sheep.legs) 
console.log(sheep.cold_blooded) 

console.log('-----Release 1-----')
class Ape extends Animal
  {
    constructor(name, legs, cold_blooded) 
    {
      super(name, legs, cold_blooded,yell());
      function yell() 
      {
          return console.log("Auooooo")
      }
    }
  }
class Frog extends Animal
  {
    constructor(name, legs, cold_blooded) 
    {
      super(name, legs, cold_blooded);
      let jump = () =>
      {
          return "hop hop"
      }
    }
  }
var sungokong = new Ape("kera sakti","2","false");
console.log(sungokong.name) 
console.log(sungokong.legs) 
console.log(sungokong.cold_blooded) 

var kodok = new Frog("buduk","4","false");
console.log(kodok.name) 
console.log(kodok.legs) 
console.log(kodok.cold_blooded) 

// Soal 2
console.log('-----Soal 2-----')
class Clock 
{
    constructor()
    {
        this.stop
        this.start
    }
    present()
    {   return
     var timer;
  
     function render() 
      {
      var date = new Date();
  
      var hours = date.getHours();
      if (hours < 10) hours = '0' + hours;
  
      var mins = date.getMinutes();
      if (mins < 10) mins = '0' + mins;
  
      var secs = date.getSeconds();
      if (secs < 10) secs = '0' + secs;
  
      var output = template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);
  
      console.log(output);
      }
      {
        this.stop = function() 
        {
          clearInterval(timer);
        };
      }
      {
        this.start = function render() 
        {
          render();
          timer = setInterval(render, 1000);
        };
      }
    }
}
var clock = new Clock({template: 'h:m:s'});
clock.start();

