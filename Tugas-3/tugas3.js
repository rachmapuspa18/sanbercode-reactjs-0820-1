// Soal 1
var kata_1 = "saya";
var kata_2 = " senang";
var kata_3 = " belajar";
var kata_4 = " javascript".toUpperCase();

var baru = kata_1+kata_2+kata_3+kata_4;

console.log(baru);

// Soal 2
var angka1 = "1";
var angka2 = "2";
var angka3 = "4";
var angka4 = "5";

var a1 = parseInt(angka1);
var a2 = parseInt(angka2);
var a3 = parseInt(angka3);
var a4 = parseInt(angka4);

var hasil = a1+a2+a3+a4;

console.log(hasil);

// Soal 3
var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4,14);  
var kataKetiga = kalimat.substring(15,18); 
var kataKeempat = kalimat.substring(19,24); 
var kataKelima = kalimat.substring(25,31); 

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);

// Soal 4
var nilai = 69

if (nilai >=80 && nilai <=100)
{
    console.log("A")
} 
else if (nilai >=70 && nilai <80)
{
    console.log("B") 
}
else if (nilai >=60 && nilai <70)
{
    console.log("c")
} 
else if (nilai >=50 && nilai <60)
{
    console.log("D")
}
else 
{
    console.log("E")
}

// Soal 5
var hari = '18';
var bulan = 11;
var tahun = ' 1999';
switch(bulan) 
{
  case 1:   { console.log(hari+' Januari'+tahun); break; }
  case 2:   { console.log(hari+' Februari'+tahun); break; }
  case 3:   { console.log(hari+' Maret'+tahun); break; }
  case 4:   { console.log(hari+' April'+tahun); break; }
  case 5:   { console.log(hari+' Mei'+tahun); break; }
  case 6:   { console.log(hari+' Juni'+tahun); break; }
  case 7:   { console.log(hari+' Juli'+tahun); break; }
  case 8:   { console.log(hari+' Agustus'+tahun); break; }
  case 9:   { console.log(hari+' September'+tahun); break; }
  case 10:   { console.log(hari+' Oktober'+tahun); break; }
  case 11:   { console.log(hari+' November'+tahun); break; }
  case 12:   { console.log(hari+' Desember'+tahun); break; }
} 