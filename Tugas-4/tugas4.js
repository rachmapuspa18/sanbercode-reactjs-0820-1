// Soal 1
// LOOPING PERTAMA
var mulai = 1;
var deret = 1;
console.log('LOOPING PERTAMA'); 

while(mulai < 21) 
{ 
  mulai += deret;
  console.log(mulai +' - I love coding' ); 
  mulai++; 
}
// LOOPING KEDUA
var mulai = 21;
var deret = 1;
console.log('LOOPING KEDUA'); 

while(mulai > 2) 
{ 
  mulai -= deret;
  console.log(mulai +' - I will become a fronted developer' ); 
  mulai--; 
}

// Soal 2
console.log('------------------------------------------------');
for (var angka = 1; angka < 21; angka++)
{
 if(angka%2==0)
 {
   console.log(angka + ' - Berkualitas');
 }
 else if(angka%3==0)
 {
   console.log(angka + ' - I Love Coding');
 }
 else 
 {
   console.log(angka + ' - Santai');
 }
}

// Soal 3
console.log('------------------------------------------------');
var spasi = " ";
for (var i=0; i<=7; i++)
{
    for (var k=1; k<=i; k++)
    {
        spasi+="#";
    }
    spasi +='\n';
}
console.log(spasi);

// Soal 4
console.log('------------------------------------------------');
var kalimat="saya sangat senang belajar javascript"
var baru = kalimat.split(" ")
console.log(baru)

// Soal 5
console.log('------------------------------------------------');
var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
daftarBuah.sort()
var yow = daftarBuah.join('')
var mangga = yow.substring(0, 9); 
var apel = yow.substring(9,16);  
var anggur = yow.substring(16,25); 
var semangka = yow.substring(25,36); 
var jeruk = yow.substring(36,46);

console.log(mangga);
console.log(apel);
console.log(anggur);
console.log(semangka);
console.log(jeruk);