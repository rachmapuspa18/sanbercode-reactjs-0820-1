// Soal 1
console.log('-----Soal 1-----')
const phi = 3.14;
let luas = (x) =>
{
    return phi*x*x
}
let keliling = (x) =>
{
    return phi*(x+x)
}
let num1 = 18
let hasilluas = luas(num1)
let hasilkeliling = keliling(num1)
console.log(hasilluas) 
console.log(hasilkeliling) 

// Soal 2
console.log('-----Soal 2-----')
let kalimat = ""
let tambahkalimat = () =>
{
    kalimat.push({kata1, kata2, kata3, kata4, kata5})
}
const kata1 = 'saya'
const kata2 = 'adalah'
const kata3 = 'seorang'
const kata4 = 'fronted'
const kata5 = 'developer'
 
const theString = `${kata1} ${kata2} ${kata3} ${kata4} ${kata5}`
console.log(theString) 

// Soal 3 
console.log('-----Soal 3-----')
let newFunction = ""
let function_literal = () =>
{
    newFunction.push({firstname, lastname})
}
const firstname = 'William'
const lastname = 'Imoh'

const fullName = `${firstname} ${lastname}`
console.log(fullName) 

// Soal 4
console.log('-----Soal 4-----')
const newObject = 
{
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}

const {firstName, lastName, destination, occupation, spell} = newObject
console.log(firstName, lastName, destination, occupation)

// Soal 5
console.log('-----Soal 5-----')
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

const combined = [...west, ...east]
console.log(combined)